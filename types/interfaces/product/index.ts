export interface Brand {
    id: string,
    name: string,
    slug: string
}

export interface Category {
    id: string,
    name: string,
    title: string,
    slug: string
}

export interface Picture {
    thumbnail: string,
    large: string
}

export interface Product {
    id: string,
    title: string,
    slug: string,
    brand: Brand,
    category: Category,
    picture: Picture,
    created_at: string,
    views: number,
    is_exist: boolean,
    discounted_price: number,
    price: number,
    discount: number,
    discountDifference: number
}

export interface API {
    next: string
    previous: string | null
    results: Product[]
}